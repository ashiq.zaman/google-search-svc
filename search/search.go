package search

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/gocolly/colly/v2"
	"github.com/gocolly/colly/v2/queue"
	"golang.org/x/time/rate"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

var RateLimit *rate.Limiter

type Result struct {
	Id               int    `json:"id"`
	Keyword          string `json:"keyword"`
	AdsCount         int    `json:"ads_count"`
	LinksCount       int    `json:"links_count"`
	TotalSearchCount int    `json:"total_search_count"`
	Page             string `json:"page"`
	Status           string `json:"status"`
	Error            string `json:"error"`
}

const googleBaseUrl = "https://www.google.com/search?hl=en&q="
const defaultAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"

func Search(ctx context.Context, searchResult *Result) error {
	if ctx == nil {
		ctx = context.Background()
	}

	if err := RateLimit.Wait(ctx); err != nil {
		return err
	}

	c := colly.NewCollector(colly.MaxDepth(1))
	httpTransport := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true, // Skip certificate verification
		},
	}
	
	httpClient := &http.Client{
		Transport: httpTransport,
	}

	customTransport := &customRoundTripper{
		Transport: httpClient.Transport,
	}
	c.WithTransport(customTransport)

	c.UserAgent = defaultAgent
	q, _ := queue.New(1, &queue.InMemoryQueueStorage{MaxSize: 10000})

	var rErr error

	c.OnError(func(r *colly.Response, err error) {
		rErr = err
	})

	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		searchResult.LinksCount++
	})

	c.OnHTML("#tads a[href]", func(e *colly.HTMLElement) {
		searchResult.AdsCount++
	})
	c.OnResponse(func(r *colly.Response) {
		searchResult.Page = string(r.Body)
	})
	c.OnHTML("#result-stats", func(e *colly.HTMLElement) {
		searchCountStr := strings.TrimSpace(e.Text)
		re := regexp.MustCompile(`[\d,]+`)
		match := re.FindString(searchCountStr)
		match = strings.ReplaceAll(match, ",", "")
		count, err := strconv.Atoi(match)
		if err != nil {
			log.Println("Failed to convert search count:", err)
			return
		}
		searchResult.TotalSearchCount = count
	})

	url := buildUrl(searchResult.Keyword)
	q.AddURL(url)
	q.Run(c)

	if rErr != nil {
		log.Println("error: ", rErr)
		searchResult.Status = "failed"
		searchResult.Error = rErr.Error()
	}
	return nil
}

func buildUrl(searchTerm string) string {
	searchTerm = strings.Trim(searchTerm, " ")
	searchTerm = strings.Replace(searchTerm, " ", "+", -1)
	return fmt.Sprintf("%s%s", googleBaseUrl, searchTerm)
}

type customRoundTripper struct {
	Transport http.RoundTripper
}

func (c *customRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	return c.Transport.RoundTrip(req)
}
