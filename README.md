# Google Search Service

Google search service for [bulk searcher](https://gitlab.com/ashiq.zaman/bulk-searcher)

### Requirements

- Golang 1.18
- Docker

### Installation and Run

1. Clone the repository:

```bash
git clone git@gitlab.com:ashiq.zaman/google-search-svc.git
cd google-search-svc
```

2. Install dependencies and build

```bash
go mod download
go build -o google-search 
```

3. Run

```bash
export KAFKA_URL=192.168.232.1:9092
export READ_TOPIC_NAME=google_scheduled_search_raw
export WRITE_TOPIC_NAME=google_scheduled_search_done
export GROUP_ID=google_search_service
export SEARCH_LIMIT_PER_MINUTE=5
./google-search
```

#### Docker Build and push to dockerhub

```bash
./hacks/scripts/dockerhub-push.sh
```

### Run using docker

```bash
docker run -t \
    -e KAFKA_URL=192.168.232.1:9092 \
    -e READ_TOPIC_NAME=google_scheduled_search_raw \
    -e WRITE_TOPIC_NAME=google_scheduled_search_done \
    -e GROUP_ID=google_search_service \
    -e SEARCH_LIMIT_PER_MINUTE=5 \
    ashiquzzaman33/google-search-svc:latest
```

### Deployment using docker swarm
```bash
export KAFKA_URL=192.168.232.1:9092
export READ_TOPIC_NAME=google_scheduled_search_raw
export WRITE_TOPIC_NAME=google_scheduled_search_done
export GROUP_ID=google_search_service
export SEARCH_LIMIT_PER_MINUTE=5
docker stack deploy -c ./hacks/deployments/google-search-svc.yaml google-search

```