#!/bin/bash

PROJECT_DIR=$(dirname "$(dirname "$(dirname "$(readlink -f "$0")")")")
TAG_NAME="ashiquzzaman33/google-search-svc:latest"
docker build -t $TAG_NAME $PROJECT_DIR
docker push $TAG_NAME

echo "Successfully pushed $TAG_NAME"