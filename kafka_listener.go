package main

import (
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"gitlab.com/google-search-svc/search"
	"log"
	"os"
	"strings"
)

func getKafkaReader(kafkaURL, topic, groupID string) *kafka.Reader {
	brokers := strings.Split(kafkaURL, ",")
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:  brokers,
		GroupID:  groupID,
		Topic:    topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})
}

var kafkaWriter *kafka.Writer

func newKafkaWriter(kafkaURL, topic string) *kafka.Writer {
	return &kafka.Writer{
		Addr:     kafka.TCP(kafkaURL),
		Topic:    topic,
		Balancer: &kafka.LeastBytes{},
	}
}

func searchAndUpdate(ctx context.Context, result *search.Result) {
	log.Printf("searching... keyword: %v", result.Keyword)
	err := search.Search(ctx, result)
	if err != nil {
		log.Println("error: ", err)
		return
	}

	body, err := json.Marshal(result)
	if err != nil {
		log.Println("error: ", err)
		return
	}
	msg := kafka.Message{
		Value: body,
	}
	err = kafkaWriter.WriteMessages(ctx, msg)
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("search done... keyword: %v link count: %v", result.Keyword, result.LinksCount)
}

func appStart(ctx context.Context) {
	kafkaURL := os.Getenv("KAFKA_URL")
	readTopic := os.Getenv("READ_TOPIC_NAME")
	writeTopic := os.Getenv("WRITE_TOPIC_NAME")
	groupID := os.Getenv("GROUP_ID")
	log.Printf("url: %v, read topic: %v, write topic: %v, group id: %v\n", kafkaURL, readTopic, writeTopic, groupID)

	reader := getKafkaReader(kafkaURL, readTopic, groupID)
	kafkaWriter = newKafkaWriter(kafkaURL, writeTopic)

	defer reader.Close()
	log.Printf("start consuming from %v topic ... ", readTopic)
	for {
		result := search.Result{}
		m, err := reader.ReadMessage(context.Background())
		if err != nil {
			log.Fatal(err)
		}
		err = json.Unmarshal(m.Value, &result)
		if err != nil {
			log.Println("Error:", err)
			continue
		}
		go searchAndUpdate(ctx, &result)
	}
}
