package main

import (
	"context"
	"crypto/tls"
	"gitlab.com/google-search-svc/search"
	"golang.org/x/time/rate"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

func main() {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	limitPerMinute, err := strconv.Atoi(os.Getenv("SEARCH_LIMIT_PER_MINUTE"))
	if err != nil {
		log.Fatal("invalid search limit per minute:", err)
		return
	}
	search.RateLimit = rate.NewLimiter(rate.Every(time.Minute), limitPerMinute)
	appStart(context.Background())
}
